<%@ page import="java.util.List" %>
<%@ page import="Models.House" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="Database.Database" %><%--
  Created by IntelliJ IDEA.
  User: milad
  Date: 2/15/18
  Time: 11:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Search Results</title>
</head>
<body>
<div style="text-align: right">
    <p>
        <%
            String a = Database.get_username();
            String b = Database.get_balance();
        %>
          <%=a%><br>
        اعتبار : <%=b%><br>
    </p>
    <form action="/success.jsp">
        <input type="submit" value="بازگشت به صفحه اصلی">
    </form>
</div>
<%
    String houses = (String) request.getAttribute("houses");
    String delim = "[+]";
    String delim2 = "[$]";

    String[] h;
    if ((boolean) request.getAttribute("searchEmpty")) {
        h = new String[0];
    %>
    <p style="font-style: italic; font-family: fantasy;" >We couldn't find suitable house for you!</p>
    <%
    }
    else
        h = houses.split(delim);

    for (String house : h){
        String[] t = house.split(delim2);
    String type = t[3].equals("1")? "رهن و اجاره": t[3].equals("0")?"فروش": t[3];
%>
    <div>
قیمت پایه:         <%=t[0]%><br>
        مبلغ اجاره: <%=t[1]%><br>
        متراژ: <%=t[2]%><br>
        نوع: <%=type%><br>
        لینک عکس: <a href="<%=t[4]%>" style="text-decoration: none"> اینجا </a>  <br>
        <a href="/moreInfo?id=<%=t[5]%>"><button >اطلاعات بیشتر</button></a>
    </div>
<%}%>
    </body>
</html>
