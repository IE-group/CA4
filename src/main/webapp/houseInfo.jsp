<%@ page import="Database.Database" %><%--
  Created by IntelliJ IDEA.
  User: milad
  Date: 2/16/18
  Time: 12:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>اطلاعات خانه</title>
</head>
<body>
<div style="text-align: right">
    <p>
        <%
            String a = Database.get_username();
            String b = Database.get_balance();
        %>
        <%=a%><br>
        اعتبار : <%=b%><br>
    </p>
    <form action="/success.jsp">
        <input type="submit" value="بازگشت به صفحه اصلی">
    </form>
    <%--<form action="/SearchServlet">--%>
        <%--<input type="submit" value="بازگشت به لیست خانه ها">--%>
    <%--</form>--%>
</div>
<%
    String info = (String) request.getAttribute("info");
    String delim2 = "[$]";
    String[] t = info.split(delim2);
    String type = t[0].equals("0") ?  "ویلایی":"آپارتمانی";
%>
<div>
    نوع قرارداد:
    <%
        if (t[2].equals("0")) {
    %>
    فروش <br>
    قیمت فروش: <%=t[4]%><br>
    <%
    } else {
    %>
    اجاره<br>
    قیمت پایه: <%=t[3]%><br>
    قیمت اجاره: <%=t[4]%><br>
    <%
        }
    %>
    متراژ: <%=t[1]%><br>
    نوع: <%=type%><br>

    آدرس: <%=t[5]%><br>
    لینک عکس: <a href="<%=t[6]%>" style="text-decoration: none"> اینجا </a> <br>
    توضیحات: <%=t[7]%>  <br>
    <% if ((boolean) request.getAttribute("paid")) {
    %>
    شماره تلفن مالک/مشاور :‌
    <%=request.getAttribute("phone")%>
    <%
    } else if ((boolean) request.getAttribute("afford")) {
    %>

    <form action="/buyPhone">
        <input type="hidden" value="<%=t[8]%>" name="id">
        <input type="submit" value="دریافت شماره ی مشاور">
    </form>
    <%
    } else {
    %>
    اعتبار شما کافی نیست!<br>
    <a href="increase.jsp"><input type="button" value="افزایش اعتبار"></a>
    <%
        }
    %>
    <%--<form action="/getapi">--%>
    <%--<input type="submit" value="بازگشست به صفحه ی اول">--%>
    <%--</form>--%>

</div>
</body>
</html>
