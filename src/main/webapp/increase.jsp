<%--
  Created by IntelliJ IDEA.
  User: milad
  Date: 2/13/18
  Time: 8:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/generic.css">
    <link rel="stylesheet" href="css/Increase_style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <title>افزایش اعتبار</title>
</head>
<body>
<%@ include file="navBar.jsp" %>

<%
    String h_id;
    try {
        h_id = request.getParameter("house_id");
    } catch (Exception e) {
        h_id = "";
    }
%>
<h3 class="header"> افزایش موجودی : </h3>

<div class="right">
    <p class="right">اعتبار کنونی</p>
    <p class="left"> <%=Database.get_balance()%></p>
</div>
<form action="/increase" method="post">
    <input type="hidden" name="house_id" value="<%=h_id%>">
    <dev class="l">
        <p class="currency">
            تومان<br>
        </p>
        <input type="text" class="textInput " name="money" placeholder="مبلغ مورد نظر">
    </dev>
    <input class="button" type="submit" value="افزایش اعتبار">
    <%@ include file="footer.jsp" %>
</form>
</body>
</html>
