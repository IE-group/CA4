<%@ page import="Models.*" %>
<%@ page import="Database.Database" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="css/generic.css">

    <title>ورود موفقیت آمیز</title>
</head>
<body>
<div style="text-align: right">
<p>
  <%
    String a = Database.get_username();
    String b = Database.get_balance();
  %>
     خوش آمدید <%=a%><br>
    اعتبار : <%=b%><br>
</p>
</div>
<div style="text-align:center">
<form action="/increase" method="post">
    <input type="text" name="money">
    <input type="submit" value="افزایش اعتبار">
</form>
<form action="/search">
    <select name="buildingType">
        <option value="ویلایی">ویلایی</option>
        <option value="آپارتمان">آپارتمان</option>
        <option value="">هر دو</option>

    </select><br>
    <select name="contractType">
        <option value="0">فروش</option>
        <option value="1">رهن و اجاره</option>
        <option value="">هر دو</option>
    </select><br>
    <input type="text" name = "minArea" placeholder="حداقل متراژ"><br>
    <input type="text" name = "buildingType" placeholder="نوع ساختمان"><br>
    <!--<input type="text" name = "contractType" placeholder="نوع قرارداد"><br>-->
    <input type="text" name = "maxPrice" placeholder="حداکثر قیمت"><br>
    <input type="submit" value="جست و جو خانه">
</form>
<form action="/addHouse" method="post">
    <!--<input type="text" name="buildingType" placeholder="نوع ساختمان"><br>-->
    <select name="buildingType">
        <option value="0">ویلایی</option>
        <option value="1">آپارتمان</option>
    </select><br>
    <select name="dealType">
        <option value="0">فروش</option>
        <option value="1">رهن و اجاره</option>
    </select><br>
    <input type="text" name="area" placeholder="متراژ"><br>
    <!--<input type="text" name="dealType" placeholder="نوع قرارداد"><br>-->
    <!--<input type="text" name="sellPrice" placeholder="قیمت فروش"><br>-->
    <input type="text" name="price" placeholder="قیمت فروش/اجاره"><br>
    <%--<input type="text" name="rentPrice" placeholder="اجاره بها"><br>--%>
    <input type="text" name="phone"     placeholder="تلفن"><br>
    <input type="text" name="address" placeholder="آدرس"><br>
    <input type="text" name="description" placeholder="توضیحات"><br>
    <input type="submit" value="اضافه‌‌کردن خانه جدید">
</form>
</div>
    <!--<button style="padding: 0px;"><a style="margin: 0px;text-decoration: none;" href="/increase.jsp">افزایش اعتبار</a> </button>
    <button style="padding: 0px;"><a style="margin: 0px;text-decoration: none;" href="/addHouse.jsp"> اضافه کردن خانه </a> </button>
    <button style="padding: 0px;"><a style="margin: 0px;text-decoration: none;" href="/search.jsp"> جست و جو </a> </button>-->

</body>
</html>
