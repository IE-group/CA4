<%--
  Created by IntelliJ IDEA.
  User: milad
  Date: 2/14/18
  Time: 4:51 PM
  To change this template use File | Settings | File Templates.
--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <title>اضافه کردن خانه</title>
    <meta charset="utf-8">
</head>
<body>
    <form action="/addHouse" method="post">
        <!--<input type="text" name="buildingType" placeholder="نوع ساختمان"><br>-->
        <select name="buildingType">
            <option value="ویلایی">ویلایی</option>
            <option value="آپارتمان">آپارتمان</option>
        </select><br>
        <input type="text" name="area" placeholder="متراژ"><br>
        <input type="text" name="dealType" placeholder="نوع قرارداد"><br>
        <input type="text" name="sellPrice" placeholder="قیمت فروش"><br>
        <input type="text" name="basePrice" placeholder="قیمت پایه"><br>
        <input type="text" name="rentPrice" placeholder="اجاره بها"><br>
        <input type="text" name="phone"     placeholder="تلفن"><br>
        <input type="text" name="address" placeholder="آدرس"><br>
        <input type="text" name="description" placeholder="توضیحات"><br>
        <input type="submit" value="اضافه‌‌کردن">
    </form>
</body>
</html>
