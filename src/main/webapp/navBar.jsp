<%@ page import="Database.Database" %><%--
  Created by IntelliJ IDEA.
  User: milad
  Date: 2/28/18
  Time: 11:48 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<nav>
    <section class="right">
        <img src="icons/logo.png" class="logo">
        <h5>خانه به دوش</h5>
    </section>

    <section class="left personalInfo">

        <div class="dropdown">
            <p class="fa fa-smile-o"></p>
            <h5>ناحیه ی کاربری</h5>
            <div class="dropdown-content">
                <p class="nameDecoration"><%= Database.get_username() %><br>
                </p>
                <div>
                    <p class="credit right">اعتبار </p>
                    <p class="credit left"> <%= Database.get_balance() %>    تومان
                    </p>
                    <a href="increase.jsp"><input type="button" class="button increaseButton" value="افزایش اعتبار"></a>
                </div>
            </div>
        </div>
    </section>
</nav>
