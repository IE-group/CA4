package Utils;

import Models.Individual;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;

public class Util {

    public static String Hash_Password(String pass){
//        String password = "123456";

        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        md.update(pass.getBytes());

        byte byteData[] = md.digest();

        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

//        System.out.println("Digest(in hex format):: " + sb.toString());
        return  sb.toString();
    }

    public static String Handle_XSS(String str){
        str =str.replaceAll("<","&lt;");
        str =str.replaceAll(">","&gt;");
        str =str.replaceAll("&","&amp;");
        str =str.replaceAll("'","&#39");
        str =str.replaceAll("\"","&quot;");
        return str;
    }

    public static boolean check_house_is_paid(int userId, String houseId) {
        Connection conn = null;
        try {
            conn = DataSource.getConnection();
            String q = "SELECT * FROM House_User WHERE userId = ? And houseId = ?";
            PreparedStatement st = conn.prepareStatement(q);

            st.setInt(1, userId);
            st.setString(2, houseId);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                close_connection(conn);
                return true;
            } else {
                close_connection(conn);
                return false;
            }

        } catch (SQLException e) {
            close_connection(conn);
            e.printStackTrace();
            return false;
        }

    }

    public static int get_balance(int userId) {
        Connection conn = null;
        try {
            conn = DataSource.getConnection();

            String q = "SELECT balance FROM User WHERE id = ? ";
            PreparedStatement st = conn.prepareStatement(q);

            st.setInt(1, userId);

            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                int res = rs.getInt(1);
                close_connection(conn);
                return res;
            } else {
                close_connection(conn);
                return 0;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }

    }

    public static JSONObject fetch_JSONObject(BufferedReader reader) throws IOException, JSONException {

        String line;
        StringBuilder buffer = new StringBuilder();

        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }

        String data = buffer.toString();

        return new JSONObject(data);
    }


    public static int get_userId(HttpServletRequest request) {
        boolean logged = (boolean) request.getAttribute("Logged");
        int userId = -1;

        if (logged)
            try {
                userId = Individual.fetch_id((String) request.getAttribute("username"));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        return userId;
    }

    public static void set_default_header(HttpServletResponse response) {
        response.setContentType("application/json");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setCharacterEncoding("utf-8");
    }

    public static int parse_number(String num) {
        int res;
        try {
            res = Integer.parseInt(num);
        } catch (NumberFormatException e) {
            res = 999999999;
        }
        return res;
    }

    public static JSONObject convert_row_to_json(ResultSet resultSet) {
        int total_rows = 0;
        JSONObject price = new JSONObject();

        try {
            total_rows = resultSet.getMetaData().getColumnCount();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        JSONObject obj = new JSONObject();
        for (int i = 0; i < total_rows; i++) {
            String columnName = null;
            try {
                columnName = resultSet.getMetaData().getColumnLabel(i + 1);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            Object columnValue = null;
            try {
                columnValue = resultSet.getObject(i + 1);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            // if value in DB is null, then we set it to default value
            if (columnValue == null) {
                columnValue = "null";
            }
            if (obj.has(columnName)) {
                columnName += "1";
            }
            try {
                if (columnName.equals("sellPrice") || columnName.equals("basePrice") || columnName.equals("rentPrice")) {
                    price.put(columnName, columnValue);
                } else
                    obj.put(columnName, columnValue);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        try {
            obj.put("price", price);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }

    public static void close_connection(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
