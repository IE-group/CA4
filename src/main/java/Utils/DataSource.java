package Utils;

import org.apache.commons.dbcp.BasicDataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DataSource {

    private static BasicDataSource datasource = new BasicDataSource();

    static {
        datasource.setDriverClassName("org.sqlite.JDBC");
        datasource.setUrl("jdbc:sqlite:Database.db");
        datasource.setDefaultAutoCommit(false);
    }

    public static Connection getConnection() throws SQLException {
        return datasource.getConnection();
    }
}