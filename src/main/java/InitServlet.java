import Models.House;
import Models.Individual;
import Models.RealState;
import Utils.DataSource;
import Utils.Util;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static Utils.Util.close_connection;
import static Utils.Util.set_default_header;

//package com.mkyong.test;

import java.security.MessageDigest;

@WebServlet(name = "InitServlet")
//Todo: addhouse
public class InitServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

    }

    private void createTable() {
        try {
            Connection connection = DataSource.getConnection();
            Statement statement = connection.createStatement();

            statement.execute(
                    "drop table if exists Realstate"
            );
            statement.executeUpdate(
                    "create table IF NOT EXISTS Realstate(" +
                            " id INTEGER PRIMARY KEY ," +
                            " name String ," +
                            " url String," +
                            " expireTime String)"
            );

            statement.execute(
                    "drop table if exists User"
            );
            statement.executeUpdate(
                    "create table IF NOT EXISTS User (" +
                            " id integer PRIMARY KEY," +
                            " name string," +
                            " phone String," +
                            " balance String, " +
                            " userName String," +
                            " Password String," +
                            " isAdmin BIT ," +
                            " isLoggedin BIT" +
                            " )"
            );

            statement.execute(
                    "drop table if exists House"
            );
            statement.executeUpdate(
                    "create table IF NOT EXISTS House (" +
                            " id String PRIMARY KEY, " +
                            " area INTEGER ," +
                            " ownerId INTEGER ," +
                            " sellPrice INTEGER ," +
                            " basePrice INTEGER ," +
                            " rentPrice INTEGER ," +
                            " buildingType String," +
                            " address String," +
                            " dealType INTEGER ," +
                            " imageURL String," +
                            " description String," +
                            " phone String," +
                            " FOREIGN KEY (ownerId) REFERENCES Realstate(id)" +
                            ")"
            );

            statement.execute(
                    "drop table if exists House_User"
            );
            statement.executeUpdate(
                    "create table IF NOT EXISTS House_User(" +
                            " houseId String," +
                            " userId INT , " +
                            " FOREIGN KEY(houseId) REFERENCES House(id) ON DELETE CASCADE," +
                            " FOREIGN KEY(userId) REFERENCES User(id) ON DELETE CASCADE" +
                            " PRIMARY KEY(houseId, userId)" +
                            ")"
            );
            connection.commit();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void initTables() {
        String hash_pass = Util.Hash_Password("123456");

        Individual m1 = new Individual(1, "بهنام همایون", "bh", false, false,hash_pass);
        m1.save();
        Individual m2 = new Individual(2, "حسین تن", "ht", true, false,hash_pass);
        m2.save();
        Individual m3 = new Individual(3, "حسان گورابی", "hg", false, false,hash_pass);
        m3.save();
        Individual m4 = new Individual(4, "وحید یوسفی", "waheed", false, false,hash_pass);
        m4.save();
        RealState r = new RealState(0, "default", "", "");
        r.save();
        RealState r1 = new RealState(1, "KhaneBeDoosh", "http://139.59.151.5:6664/khaneBeDoosh/v2/house", "");
        r1.save();
        House h = new House("1", "ویلایی", 350, 0, 200, 200, 200, "0123548654", "1", "خانه ی امید", "", 0);
        Connection connection = null;
        try {
            connection = DataSource.getConnection();

            h.save(connection);
            connection.commit();
            close_connection(connection);
            House.purchase_phone(1, "1", true);


        } catch (SQLException e) {
            close_connection(connection);
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        createTable();
        initTables();
        set_default_header(response);
        JSONObject result = new JSONObject();
        try {
            result.put("status", "OK");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        response.setStatus(200);
        response.getWriter().print(result.toString());
    }
}
