import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

class Sample {
    public static void main() {
        Connection connection = null;
        try {
            Class.forName("org.sqlite.JDBC");

            // create a database connection
            connection = DriverManager.getConnection("jdbc:sqlite:Database.db");
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

//            statement.executeUpdate("drop table if exists User");
            statement.executeUpdate("create table IF NOT EXISTS User (id integer, firstName string, lastName String)");
            statement.executeUpdate("insert into User values('1', 'بهنام', 'همایون')");
            ResultSet rs = statement.executeQuery("select * from User");
            while (rs.next()) {
                // read the result set
                System.out.println("name = " + rs.getString("firstName"));
                System.out.println("id = " + rs.getInt("id"));
            }
        } catch (SQLException e) {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                // connection close failed.
                System.err.println(e);
            }
        }
    }
}