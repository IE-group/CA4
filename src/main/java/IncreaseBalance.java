import Utils.DataSource;
import Utils.Util;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.*;

import static Utils.Util.close_connection;
import static Utils.Util.parse_number;


@WebServlet(name = "IncreaseBalance")
public class IncreaseBalance extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Util.set_default_header(response);
        String money, apiKey = "60ffead0-12e7-11e8-87b4-496f79ef1988", line;
        int userId = Util.get_userId(request);
        JSONObject item = new JSONObject(), o;

        if (userId == -1) {
            try {
                item.put("result", "NOK");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            response.setStatus(403);
            response.getWriter().print(item.toString());
            return;
        }

        StringBuilder buffer = new StringBuilder();
        int new_balance = 0;

        BufferedReader reader = request.getReader();

        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }

        String data = buffer.toString();

        try {
            o = new JSONObject(data);
            money = o.getString("money");
            if (money.equals(""))
                money = "0";
            if (Integer.parseInt(money) < 0) {
                throw new Exception();
            }
        } catch (Exception e) {
            try {

                item.put("result", "NOK");
                response.setStatus(500);
                response.getWriter().print(item.toString());

            } catch (JSONException e1) {
                e1.printStackTrace();
            }
            return;
        }

        String ACMBank = "http://139.59.151.5:6664/bank/pay";
        URL url = new URL(ACMBank);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setConnectTimeout(10000);
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("apiKey", apiKey);
        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        response.setContentType("text/html");

        JSONObject jsonBankOut = new JSONObject();
        try {

            jsonBankOut.put("userId", "123");
            jsonBankOut.put("value", money);

        } catch (JSONException e) {
            try {
                item.put("result", "NOK");
                response.setStatus(200);
                response.getWriter().print(item.toString());

            } catch (JSONException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
            return;
        }

        OutputStreamWriter bankOutStream = new OutputStreamWriter(connection.getOutputStream());
        bankOutStream.write(jsonBankOut.toString());
        bankOutStream.flush();
        Connection conn;
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        JSONObject jsonInput;
        try {

            jsonInput = new JSONObject(in.readLine());

            if (jsonInput.getString("result").equals("OK")) {
                conn = DataSource.getConnection();

                String q = "SELECT * FROM User WHERE id = ?";
                PreparedStatement st = conn.prepareStatement(q);
                st.setInt(1, userId);

                ResultSet rs = st.executeQuery();
                st.clearParameters();
                try {
                    String old_balance = rs.getString("balance");
                    close_connection(conn);
                    conn = DataSource.getConnection();

                    new_balance = parse_number(old_balance) + parse_number(money);
                    q = "UPDATE User SET balance = ? where id = ?";

                    st = conn.prepareStatement(q);
                    st.setInt(1, new_balance);
                    st.setInt(2, userId);
                    st.executeUpdate();

                    conn.commit();
                    close_connection(conn);
                } catch (SQLException e) {
                    close_connection(conn);
                    e.printStackTrace();
                }
            }


        } catch (JSONException | SQLException e) {
            e.printStackTrace();
        }
        connection.disconnect();


        try {
            item.put("credit", Integer.toString(new_balance));
            item.put("result", "OK");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        response.setStatus(200);
        response.getWriter().print(item.toString());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {

    }
}
