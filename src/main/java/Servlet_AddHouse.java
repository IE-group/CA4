import Models.House;
import Utils.DataSource;
import Utils.Util;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static Utils.Util.*;

@WebServlet(name = "Servlet_AddHouse")
public class Servlet_AddHouse extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        set_default_header(response);
        Connection connection = null;
        Connection connection2 = null;
        int userId = Util.get_userId(request);
        if (userId == -1) {
            response.setStatus(403);
            return;
        }
        try {
            connection2 = DataSource.getConnection();

            String a = (String) request.getAttribute("username");
            String q = "SELECT * FROM User WHERE userName = ?";

            PreparedStatement st = connection2.prepareStatement(q);
            st.setString(1, a);

            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                close_connection(connection2);
                String bt;
                int dt;

                String bp = "0";

                String p;
                String add;
                String dscr;

                int area;

                int sell_price = 0;
                int rent_price = 0;
                JSONObject result = new JSONObject();

                StringBuilder buffer = new StringBuilder();
                BufferedReader reader = request.getReader();
                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                String data = buffer.toString();
                JSONObject o;

                try {
                    o = new JSONObject(data);
                    bt = Util.Handle_XSS(o.getString("buildingType"));
                    dt = o.getInt("dealType");

                    if (dt == 0) {
                        sell_price = parse_number(o.getString("sellPrice"));

                    } else if (dt == 1) {
                        rent_price = parse_number(o.getString("rentPrice"));
                        bp = Util.Handle_XSS(o.getString("basePrice"));
                    }

                    add = Util.Handle_XSS(o.getString("address"));
                    dscr = Util.Handle_XSS(o.getString("description"));
                    p = Util.Handle_XSS(o.getString("phone"));
                    area = parse_number(o.getString("area"));

                } catch (Exception e) {
                    close_connection(connection2);
                    try {
                        result.put("status", "NOK");
                    } catch (JSONException l) {
                        e.printStackTrace();
                    }

                    response.getWriter().print(result.toString());
                    return;
                }
                try {

                    connection = DataSource.getConnection();
                    int cnt = House.house_count(0) + 1;
                    String id = Integer.toString(cnt);
                    House h = new House(id, bt, area, dt, sell_price, Integer.parseInt(bp), rent_price, p, add, dscr, "", 0);
                    h.save(connection);//Adding to Real DataBase
                    connection.commit();
                    close_connection(connection);
                    House.purchase_phone(userId, id, true);


                } catch (SQLException e) {
                    close_connection(connection);
                    e.printStackTrace();
                }

                try {
                    result.put("status", "OK");
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                response.setStatus(200);
                response.getWriter().print(result.toString());


            } else {

                response.setStatus(403);
                close_connection(connection2);
            }

        } catch (SQLException e) {
            close_connection(connection2);
            response.setStatus(403);
            e.printStackTrace();
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {


    }
}
