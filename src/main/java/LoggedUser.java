import Utils.DataSource;
import Utils.Util;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.time.Instant;

import static Utils.Util.close_connection;
import static Utils.Util.set_default_header;

@WebServlet(name = "LoggedUser")
public class LoggedUser extends HttpServlet {

    public void init() {
    }

    private String JWT_gen(String username) throws UnsupportedEncodingException {
        Algorithm algorithm = Algorithm.HMAC256("mozi-amoo");
        return JWT.create()
                .withIssuer("RooziMili")
                .withIssuedAt(Date.from(Instant.ofEpochSecond(1466796822L)))
                .withClaim("username", username)
                .sign(algorithm);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String token;
        JSONObject item = new JSONObject();

        set_default_header(response);
        Connection connection = null;
        try {

            JSONObject o = Util.fetch_JSONObject(request.getReader());

            String username = o.getString("username");
            String password = o.getString("password");
            String hashed_pass = Util.Hash_Password(password);

            connection = DataSource.getConnection();

            String q = "select * from User where userName = LOWER (?) AND password = ?";
            PreparedStatement searchQuery = connection.prepareStatement(q);

            searchQuery.setString(1, username);
            searchQuery.setString(2, hashed_pass);


            ResultSet rs = searchQuery.executeQuery();

            if (rs.next()) {
                token = JWT_gen(username);
                item.put("name", rs.getString("name"));
                item.put("credit", rs.getString("balance"));
                item.put("status", "OK");
                item.put("JWTToken", token);

                response.setStatus(200);

            } else {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                item.put("status", "NOK");
            }
            close_connection(connection);
        } catch (SQLException | JSONException e1) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);

            close_connection(connection);
            e1.printStackTrace();
        }
        response.getWriter().print(item.toString());

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {

    }
}
