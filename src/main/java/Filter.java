import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

@WebFilter("/*")
public class Filter implements javax.servlet.Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpRequest = (HttpServletRequest) req;
        HttpServletResponse httpResponse = (HttpServletResponse) resp;

        String token = httpRequest.getHeader("Authorization");

        try {
            if (token != null && !token.equals("")) {
                Algorithm algorithm = Algorithm.HMAC256("mozi-amoo");
                JWTVerifier verifier = JWT.require(algorithm)
                        .withIssuer("RooziMili")
                        .build(); //Reusable verifier instance
                DecodedJWT jwt = verifier.verify(token);

                String username = jwt.getClaim("username").asString().toLowerCase();
                req.setAttribute("username", username);
                req.setAttribute("Logged", true);
                chain.doFilter(req, resp);
            }
            else {
                req.setAttribute("Logged", false);
                chain.doFilter(req, resp);
            }
        } catch (UnsupportedEncodingException exception) {
            //UTF-8 encoding not supported
        } catch (JWTVerificationException exception) {
            httpResponse.setContentType("application/json");
            httpResponse.setHeader("Access-Control-Allow-Origin", "*");
            req.setAttribute("Logged", false);
            httpResponse.setStatus(403);
//            chain.doFilter(req, resp);
        }

    }

    public void init(FilterConfig config) {

    }

}
