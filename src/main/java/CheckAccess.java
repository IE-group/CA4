import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static Utils.Util.set_default_header;

@WebServlet(name = "CheckAccess")
public class CheckAccess extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        JSONObject res = new JSONObject();
        set_default_header(response);
        try {
            if ((boolean) request.getAttribute("Logged")) {
                res.put("status", "OK");
                response.setStatus(200);

            } else {
                res.put("status", "NOK");
                response.setStatus(403);
            }
        } catch (JSONException e) {
            response.setStatus(404);
            e.printStackTrace();
        }
        response.getWriter().print(res.toString());

    }
}
