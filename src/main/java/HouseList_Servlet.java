import Models.Individual;
import Utils.DataSource;
import Utils.Util;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

import static Utils.Util.close_connection;
import static Utils.Util.convert_row_to_json;

@WebServlet(name = "HouseList_Servlet")

public class HouseList_Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Util.set_default_header(response);
        String a = (String) request.getAttribute("username");
        JSONObject finalRes = new JSONObject();
        int page = 0 , size = 20;
        try{
             page = Integer.parseInt(request.getParameter("page"));
             size = Math.min(Integer.parseInt(request.getParameter("size")),size);
        }catch (NumberFormatException e)
        {
            e.printStackTrace();

        }

        JSONArray res = new JSONArray();
        JSONObject status = new JSONObject();
        Connection connection = null;

        try {
            status.put("status", "NOK");
            status.put("found", false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        int resNum = 403;

        try {
            if ((boolean) request.getAttribute("Logged")) {
                connection = DataSource.getConnection();
                String q = "SELECT * FROM User WHERE userName = ?";
                PreparedStatement st = connection.prepareStatement(q);
                st.setString(1, a);

                int userId = Individual.fetch_id(a);
                if (userId == -1)
                    return;
                ResultSet rs = st.executeQuery();
                st.clearParameters();

                if (rs.next()) {
                    if (!rs.getBoolean("isAdmin")) {
                        q = "Select u.name , h.* From House_User hu, House h, User u " +
                                "Where hu.houseId=h.id AND hu.userId=u.id AND u.id=? " +
                            "LIMIT ? OFFSET ? ";

                        PreparedStatement searchQuery = connection.prepareStatement(q);
                        searchQuery.setInt(1, userId);
                        searchQuery.setInt(2, size);
                        searchQuery.setInt(3,page*size - 1);
                        ResultSet rs2 = searchQuery.executeQuery();
                        while (rs2.next()) {
                            res.put(convert_row_to_json(rs2));
                            JSONObject j = new JSONObject();
                            j.put("House Id ", rs2.getString(2));
                            j.put("Owner Id ", rs2.getString(3));
                            finalRes.append(rs2.getString(1), j);
                            status.put("found", true);
                        }
                        close_connection(connection);
                        resNum = 200;
                        status.put("status", "OK");
                    } else {
                        String q2 = "Select u.name , h.id, h.ownerId From House_User hu, House h, User u " +
                                "Where hu.houseId=h.id AND hu.userId=u.id " +
                                "LIMIT ? OFFSET ? ";
                        PreparedStatement searchQuery2 = connection.prepareStatement(q2);
                        searchQuery2.setInt(1, size);
                        searchQuery2.setInt(2,page*size - 1);
                        ResultSet rs3 = searchQuery2.executeQuery();

                        while (rs3.next()) {
                            res.put(convert_row_to_json(rs3));
                            JSONObject j = new JSONObject();
                            j.put("House Id ", rs3.getString(2));
                            j.put("Owner Id ", rs3.getString(3));
                            finalRes.append(rs3.getString(1), j);
                            status.put("found", true);
                        }
                        close_connection(connection);
                        resNum = 200;
                        status.put("status", "OK");
                    }
                }
            }
        } catch (SQLException | JSONException e) {
            close_connection(connection);
            response.setStatus(resNum);
            e.printStackTrace();
        }
        response.setStatus(resNum);
        res.put(status);
        response.getWriter().print(finalRes.toString());
    }
}
