package Models;

import Utils.DataSource;
import Utils.Util;

import java.sql.*;

import static Utils.Util.close_connection;

public class Individual {
    private int id;
    private String name;
    private String phone;
    private String balance;
    private String username;
    private String password;
    private String apiKey;
    private boolean isAdmin;
    private boolean isLoggedin;

    public Individual(int i, String n, String uName, boolean admin, boolean loggedin, String pass) {
        id = i;
        name = n;
        username = uName;
        balance = "0";
        password = pass;
        phone = "0938";
        isAdmin = admin;
        isLoggedin = loggedin;

    }

    public String get_name() {
        return username;
    }

    public String get_balance() {
        return balance;
    }

    public void increase_balance(String money) {
        balance = Integer.toString(Integer.parseInt(balance) + Integer.parseInt(money));
    }

    public void set_apiKey(String apk) {
        this.apiKey = apk;
    }

    public String get_apiKey() {
        return this.apiKey;
    }

    public void buy() {
        balance = Integer.toString(Integer.parseInt(balance) - 1000);
    }

    public boolean save() {
        Connection connection = null;
        try {
            connection = DataSource.getConnection();
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.
            int bit=0;
            if (isAdmin)
                bit = 1;
            String q = "insert into User values( ?, ?, ? , ?, ?, ?, ?, ?)";
            PreparedStatement st = connection.prepareStatement(q);
            st.setInt(1, id);
            st.setString(2, name);
            st.setString(3, phone);
            st.setInt(4, 2000);
            st.setString(5, username);
            st.setString(6, password);
            st.setInt(7, bit);
            st.setBoolean(8, isLoggedin);

            st.executeUpdate();
            connection.commit();
            close_connection(connection);
            return true;

        } catch (SQLException e) {
            close_connection(connection);
            e.printStackTrace();
            return false;
        }
    }
    public static int fetch_id(String username) throws SQLException {

        int id = -1;

        Connection connection = DataSource.getConnection();

        String q = "SELECT id FROM User WHERE userName = ?";
        PreparedStatement st = connection.prepareStatement(q);
        st.setString(1, username);
        ResultSet rs = st.executeQuery();

        if (rs.next())
            id = rs.getInt(1);

        close_connection(connection);

        return id;
    }
}
