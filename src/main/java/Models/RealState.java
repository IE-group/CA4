package Models;

import Utils.DataSource;

import java.sql.*;

import static Utils.Util.close_connection;

public class RealState {
    int id;
    String name;
    String expireTime;
    String url;

    public RealState(int i, String n, String u, String expTime) {
        id = i;
        name = n;
        url = u;
        expireTime = expTime;
    }

    public void setExpireTime(String time) {
        expireTime = time;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public String getUrl() {
        return url;
    }

    public static RealState get_agency(int id) throws SQLException {
        int i;
        String name, url, expirationTime;

        Connection connection = DataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet s = null;

        try {

            String q = "SELECT * FROM Realstate WHERE id = ?";
            PreparedStatement st = connection.prepareStatement(q);
            st.setInt(1, id);
            s = st.executeQuery();

            if (s.next()) {

                i = s.getInt(1);
                name = s.getString(2);
                url = s.getString(3);
                expirationTime = s.getString(4);
                RealState rs = new RealState(i, name, url, expirationTime);

                close_connection(connection);
                return rs;
            }
        } catch (SQLException e) {
            close_connection(connection);
            return null;
        }
        return null;
    }

    public void save() {
        Connection connection = null;

        try {
            String query =
                    "insert or replace into Realstate values(?, ?, ?, ?)";

            connection = DataSource.getConnection();

            PreparedStatement st = connection.prepareStatement(query);
            st.setInt(1, id);
            st.setString(2, name);
            st.setString(3, url);
            st.setString(4, expireTime);

            st.executeUpdate();

            connection.commit();
            close_connection(connection);

        } catch (SQLException e) {
            close_connection(connection);
            e.printStackTrace();
        }
    }

}
