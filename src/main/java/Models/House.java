package Models;

import Utils.DataSource;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.*;

import static Utils.Util.close_connection;

public class House {

    private Integer area;
    private Integer base_price;
    private Integer rent_price;
    private Integer sell_price;

    private String id;
    private String building_type;
    private String address;
    private String phone;
    private String description;
    private String image_url;
    private Integer deal_type;

    private int owner;

    public House(String i, String bt, Integer a, Integer dt, Integer sp, Integer bp, Integer rp, String p, String add, String dscr, String url, int oId) {
        id = i;
        building_type = bt;
        this.area = a;
        deal_type = dt;
        sell_price = sp;
        base_price = bp;
        rent_price = rp;
        phone = p;
        address = add;
        description = dscr;
        if (url.equals("")) {
            image_url = "../img/no-pic.jpg";
        } else
            image_url = url;
        owner = oId;
    }

    public JSONObject get_full_status() throws JSONException {
        JSONObject house = new JSONObject();
        JSONObject price = new JSONObject();
        house.put("buildingType", building_type);
        house.put("dealType", deal_type);
        house.put("imageURL", image_url);
        house.put("description", description);
        house.put("id", id);
        house.put("address", address);
        house.put("area", area);
        house.put("phone", phone);

        if (deal_type.equals("1")) {
            price.put("rentPrice", rent_price);
            price.put("basePrice", base_price);
        } else
            price.put("sellPrice", sell_price);
        house.put("price", price);
        return house;

    }

    public JSONObject get_partial_status() throws JSONException {
        JSONObject house = new JSONObject();
        JSONObject price = new JSONObject();
        house.put("area", area);
        house.put("id", id);
        house.put("dealType", deal_type);
        if (deal_type == 1) {
            price.put("basePrice", base_price);
            price.put("rentPrice", rent_price);
        } else {
            price.put("sellPrice", sell_price);
        }
        house.put("price", price);
        house.put("address", address);
        house.put("imageURL", image_url);
        return house;

    }


    public Integer get_deal_type() {
        return deal_type;
    }

    public int get_price() {
        return deal_type.equals("1") ? rent_price : sell_price;
    }

    public int get_ownerId() {
        return owner;
    }

    public void save(Connection connection) throws SQLException {

        String q = "insert or replace into House values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        PreparedStatement st = connection.prepareStatement(q);
        st.setString(1, id);
        st.setInt(2, area);
        st.setInt(3, owner);
        st.setInt(4, sell_price);
        st.setInt(5, base_price);
        st.setInt(6, rent_price);
        st.setString(7, building_type);
        st.setString(8, address);
        st.setInt(9, deal_type);
        st.setString(10, image_url);
        st.setString(11, description);
        st.setString(12, phone);

        st.executeUpdate();
    }

    public static House get_house(String id) {
        Connection connection = null;
        try {
            connection = DataSource.getConnection();
            String q = "select * from House where id = ?";
            PreparedStatement st = connection.prepareStatement(q);
            st.setString(1, id);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                int area = rs.getInt(2);
                int owner = rs.getInt(3);
                int sellPrice = rs.getInt(4);
                int basePrice = rs.getInt(5);
                int rentPrice = rs.getInt(6);
                String buildingType = rs.getString(7);
                String address = rs.getString(8);
                int dealType = rs.getInt(9);
                String url = rs.getString(10);
                String desc = rs.getString(11);
                String phone = rs.getString(12);
                House h = new House(id, buildingType, area, dealType, sellPrice, basePrice, rentPrice, phone, address, desc, url, owner);

                close_connection(connection);
                return h;
            }

        } catch (SQLException e) {
            close_connection(connection);
            e.printStackTrace();
        }
        return null;
    }

    public static void purchase_phone(int userId, String houseId, boolean isOwner) {
        Connection connection = null;
        try {
            connection = DataSource.getConnection();
            String q =
                    "insert into House_User values( ?, ?)";

            PreparedStatement st = connection.prepareStatement(q);

            st.setString(1, houseId);
            st.setInt(2, userId);
            st.executeUpdate();
            st.clearParameters();

            if (isOwner) {
                connection.commit();
                close_connection(connection);

            } else {

                q = "SELECT balance FROM User where id = ?";
                st = connection.prepareStatement(q);
                st.setInt(1,userId);

                ResultSet rs = st.executeQuery();
                st.clearParameters();
                if (rs.next()) {
                    int balance = rs.getInt(1);
                    balance = balance - 1000;
                    q = "UPDATE User SET balance = ? WHERE id = ?";
                    st = connection.prepareStatement(q);
                    st.setInt(1, balance);
                    st.setInt(2, userId);
                    st.executeUpdate();
                }
                connection.commit();
                close_connection(connection);
            }

        } catch (SQLException e) {
            close_connection(connection);
            e.printStackTrace();
        }

    }

    public static int house_count(int ownerId) {
        Connection connection = null;

        try {
            int res = 0;
            connection = DataSource.getConnection();

            String q = "select COUNT(*) from House where ownerId = ?";

            PreparedStatement st = connection.prepareStatement(q);
            st.setInt(1, ownerId);
            ResultSet rs = st.executeQuery();

            if (rs.next())
                res = rs.getInt(1);

            close_connection(connection);

            return res;
        } catch (SQLException e) {
            close_connection(connection);
            e.printStackTrace();
        }
        return 0;
    }
}
