package Models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class MyUser {
    private String name;
    private int id;
    private ArrayList<String> registered_house = new ArrayList<String>();

    public MyUser(int i, String N) {
        id = i;
        name = N;
    }

    public void add_house_id(String house_id) {
        registered_house.add(house_id);
    }

    public boolean search_house_id(String house_id) {
        for (String i : registered_house) {
            if (house_id.equals(i))
                return true;
        }
        return false;
    }

//    public boolean save() {
//        Connection connection = null;
//        try {
//            Class.forName("org.sqlite.JDBC");
//
//            // create a database connection
//            connection = DriverManager.getConnection("jdbc:sqlite:Database.db");
//            Statement statement = connection.createStatement();
//            statement.setQueryTimeout(30);  // set timeout to 30 sec.
//            statement.executeUpdate("drop table if exists User");
//            statement.executeUpdate("create table IF NOT EXISTS User (id integer, name string)");
//            statement.executeUpdate("insert into User values('" + id + "', '" + name + "')");
//            return true;
//
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//            return false;
//        } catch (SQLException e) {
//            e.printStackTrace();
//            return false;
//        }
//    }
}
