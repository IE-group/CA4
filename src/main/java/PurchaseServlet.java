import Models.House;
import Models.Individual;
import Utils.Util;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static Utils.Util.set_default_header;

//Todo: jwt expire time
@WebServlet(name = "PurchaseServlet")
public class PurchaseServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        set_default_header(response);
        String houseId = request.getParameter("id");
        try {
            int userId = Individual.fetch_id((String) request.getAttribute("username"));
            if (userId == -1)
                throw new SQLException();

            House.purchase_phone(userId, houseId, false);

            String address = "/moreInfo";
            request.setAttribute("id", houseId);
            RequestDispatcher dispatcher = request.getRequestDispatcher(address);
            dispatcher.forward(request, response);


        } catch (SQLException e) {
            response.setStatus(403);
            e.printStackTrace();
        }
    }
}