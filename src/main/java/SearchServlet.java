import Models.House;
import Models.RealState;
import Utils.DataSource;
import Utils.Util;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.*;
import java.util.Objects;

import static Utils.Util.close_connection;
import static Utils.Util.parse_number;

//Todo: we should add ability to parse more than json
@WebServlet(name = "SearchServlet")
public class SearchServlet extends HttpServlet {
    private int min_area;
    private int max_price;
    private String building_type;
    private int contract_type;

    private House get_houses_partial_info(JSONObject o, int ownerId) throws JSONException {

        String id;

        int base_price;
        int sellPrice;
        int rentPrice;

        int area = o.getInt("area");

        String bt = o.getString("buildingType");
        Integer dt = (o.getInt("dealType"));
        JSONObject price = o.getJSONObject("price");


        try {
            sellPrice = o.getJSONObject("price").getInt("sellPrice");

        } catch (Exception e) {
            sellPrice = 999999999;
        }
        try {
            rentPrice = o.getJSONObject("price").getInt("rentPrice");
        } catch (Exception e) {
            rentPrice = 999999999;
        }
        try {
            base_price = price.getInt("basePrice");
        } catch (JSONException e) {
            base_price = 999999999;
        }

        try {
            id = o.getString("id");
        } catch (JSONException e) {
            id = Integer.toString(o.getInt("id"));
        }

        return new House(id, bt, area, dt, sellPrice, base_price, rentPrice, "0", o.getString("address"), ".", o.getString("imageURL"), ownerId);
    }

    private JSONArray parse_response(JSONArray res, JSONArray array, int ownerId) throws JSONException {
        String bt;
        int l = array.length();
        Connection connection = null;
        try {
            connection = DataSource.getConnection();

            for (int i = 0; i < l; i++) {
                JSONObject o = array.getJSONObject(i);

                House newHouse = get_houses_partial_info(o, ownerId);
                newHouse.save(connection);


                int area = o.getInt("area");
                if ((area >= min_area || min_area == 0) &&
                        (contract_type == newHouse.get_deal_type()) &&
                        ((contract_type == 0 && newHouse.get_price() <= max_price) || (contract_type == 1 && newHouse.get_price() <= max_price)) &&
                        (Objects.equals(building_type, "") || Objects.equals(building_type, o.getString("buildingType")))) {

                    switch (building_type) {
                        case "ویلایی":
                            bt = "0";
                            break;
                        case "آپارتمان":
                        case "آپارتمانی":
                            bt = "1";
                            break;
                        default:
                            bt = "2";
                            break;
                    }
                    o.put("buildingType", bt);
                    res.put(o);
                }
            }

            connection.commit();
            close_connection(connection);

            return res;

        } catch (SQLException e) {
            close_connection(connection);
            e.printStackTrace();
            return null;
        }

    }

    private int agency_count() {
        int res = 0;
        Connection connection = null;
        try {

            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:Database.db");

            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.
            ResultSet rs;
            rs = statement.executeQuery(
                    "SELECT COUNT(*) FROM Realstate"
            );
            while (rs.next()) {
                res = rs.getInt(1);
            }
            close_connection(connection);

            return res;


        } catch (ClassNotFoundException | SQLException e) {
            close_connection(connection);
            e.printStackTrace();
            return -1;
        }
    }


    private JSONArray search_other_agencies(JSONArray res) throws IOException, JSONException, SQLException {
        int agencyCount = agency_count();

        String expireTime;

        for (int i = 1; i < agencyCount; i++) {

            RealState rs = RealState.get_agency(i);
            if (rs == null)
                break;

            expireTime = rs.getExpireTime();
            long sysTime = System.currentTimeMillis();

            if (expireTime.equals("") || Long.parseLong(expireTime) - sysTime < 0) {

                URL agency = new URL(rs.getUrl());
                URLConnection yc = agency.openConnection();
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(
                                yc.getInputStream()));
                String inputLine = in.readLine();

                JSONObject jsonObj = new JSONObject(inputLine);

                long exppTime = jsonObj.getLong("expireTime");
                String expTime = Long.toString(exppTime);

                rs.setExpireTime(expTime);
                rs.save();

                JSONArray array = jsonObj.getJSONArray("data");

                in.close();
                parse_response(res, array, i);
            } else
                search_in_db(res, i);

        }
        return res;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

    }


    private JSONArray search_in_db(JSONArray res, int ownerId) {
        Connection connection = null;
        try {
            connection = DataSource.getConnection();
            Statement statement = connection.createStatement();

            String query = String.format(
                    "SELECT * FROM House WHERE (ownerId = %d) AND (area >= %d) AND (%d = dealType) " +
                            "AND ((%d = %d AND sellPrice <= %d) OR (%d = %d AND rentPrice <= %d)) " +
                            "AND ('%s' = '%s' OR '%s' = buildingType)",
                    ownerId, min_area, contract_type, contract_type, 0, max_price, contract_type, 1, max_price, building_type, "", building_type
            );

            ResultSet rs = statement.executeQuery(query);
            while (rs.next())
                res.put(Util.convert_row_to_json(rs));

            close_connection(connection);

            return res;

        } catch (SQLException e) {
            close_connection(connection);
            e.printStackTrace();
            return null;

        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Util.set_default_header(response);
        JSONObject item = new JSONObject();
        JSONArray res = new JSONArray();
        JSONArray all = new JSONArray();

        String min = request.getParameter("minArea");
        if (Objects.equals((min), ""))
            min_area = 0;
        else
            min_area = parse_number(min);

        String maxPrice = request.getParameter("maxPrice");
        if (Objects.equals((maxPrice), ""))
            max_price = 10000000;
        else
            max_price = parse_number(maxPrice);

        building_type = request.getParameter("houseType");
        if (building_type.equals("هردو"))
            building_type = "";
        String d_type = request.getParameter("dealType");
        if (Objects.equals((d_type), ""))
            contract_type = 0;
        else
            contract_type = Integer.parseInt(d_type);

        //search for our added houses
        search_in_db(res, 0);

        try {
            all = search_other_agencies(res);
        } catch (JSONException | SQLException e) {
            e.printStackTrace();
        }

        if (all.length() == 0) {
            try {
                item.put("isFound", false);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            try {
                item.put("isFound", true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try {
            item.put("result", "OK");
            item.put("houses", all.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        response.setStatus(200);
        response.getWriter().print(item.toString());
    }
}

