import Models.House;
import Models.RealState;
import Utils.Util;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;

@WebServlet(name = "HouseInfoServlet")
public class HouseInfoServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

    }

    private JSONObject get_houses_full_info(String houseId) throws JSONException, IOException, SQLException {
        House h = House.get_house(houseId);
        if (h == null)
            throw new SQLException();

        int ownerId = h.get_ownerId();
        RealState rs = RealState.get_agency(ownerId);

        if (rs == null)
            throw new SQLException();

        if (rs.getUrl().equals(""))
            return h.get_full_status();

        String url = rs.getUrl() + "/" + houseId;

        URL agency = new URL(url);
        URLConnection yc = agency.openConnection();
        InputStreamReader isr = new InputStreamReader(yc.getInputStream());
        BufferedReader in = new BufferedReader(isr);
        String inputLine;

        inputLine = in.readLine();
        JSONObject jsonObj = new JSONObject(inputLine);

        in.close();

        return jsonObj.getJSONObject("data");
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Util.set_default_header(response);
        int responseNum = 200;
        boolean logged = false;
        int userId = Util.get_userId(request);
        if (userId == -1)
            responseNum = 401;
        else {
            logged = true;
        }
        JSONObject status;
        String fakePhone;
        String id;
        try {

            id = request.getParameter("id");
        } catch (Exception e) {
            id = (String) request.getAttribute("id");
        }
        try {
            status = get_houses_full_info(id);
        } catch (JSONException e) {
            e.printStackTrace();
            status = null;
        } catch (SQLException e) {
            status = null;
            e.printStackTrace();
        }
        try {

            if (status != null) {

                String phone = status.getString("phone");
                try {
                    fakePhone = phone.substring(phone.length() - 3, phone.length()) + "******" + phone.substring(0, 1);
                }catch (StringIndexOutOfBoundsException e){
                    fakePhone = phone.substring(0,1) + "********";
                }
                status.put("status", "OK");

            } else {
                status = new JSONObject();
                status.put("status", "NOK");
                response.setStatus(404);
                response.getWriter().println(status.toString());
                return;
            }
            if (logged && Util.check_house_is_paid(userId, id))
                status.put("paid", true);

            else {

                status.put("paid", false);
                status.put("phone", fakePhone);

                if (logged && Util.get_balance(userId) < 1000)
                    status.put("afford", false);
                else
                    status.put("afford", true);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        response.setStatus(responseNum);
        response.getWriter().println(status.toString());


    }
}
