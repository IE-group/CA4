import Utils.DataSource;
import Utils.Util;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

import static Utils.Util.close_connection;
import static Utils.Util.set_default_header;

@WebServlet(name = "zzz")
public class zzz extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        set_default_header(response);
        JSONObject item = new JSONObject();
        Connection connection = null;
        try {
            connection = DataSource.getConnection();

            String a = (String) request.getAttribute("username");
            String q = "SELECT * FROM User WHERE userName = ?";

            PreparedStatement st = connection.prepareStatement(q);
            st.setString(1, a);

            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                item.put("name", rs.getString("name"));
                item.put("credit", rs.getString("balance"));
                item.put("result", "OK");
                connection.close();
            }
            else {
                connection.close();
                throw new SQLException();
            }
        } catch (JSONException | SQLException e) {
            response.setStatus(403);
            close_connection(connection);

            e.printStackTrace();
        }

        response.setCharacterEncoding("utf-8");
        response.setStatus(200);
        response.getWriter().print(item.toString());

    }
}
